package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

}

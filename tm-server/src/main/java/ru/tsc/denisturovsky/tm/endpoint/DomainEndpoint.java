package ru.tsc.denisturovsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.denisturovsky.tm.api.service.IDomainService;
import ru.tsc.denisturovsky.tm.api.service.IServiceLocator;
import ru.tsc.denisturovsky.tm.dto.request.*;
import ru.tsc.denisturovsky.tm.dto.response.*;
import ru.tsc.denisturovsky.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.tsc.denisturovsky.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IDomainService getDomainService() {
        return this.getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse backupLoadData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse backupSaveData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse base64LoadData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse base64SaveData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse binaryLoadData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse binarySaveData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadFasterXmlResponse jsonLoadFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveFasterXmlResponse jsonSaveFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadJaxBResponse jsonLoadJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveJaxBResponse jsonSaveJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadFasterXmlResponse xmlLoadFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveFasterXmlResponse xmlSaveFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlLoadJaxBResponse xmlLoadJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlSaveJaxBResponse xmlSaveJaxBData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlLoadFasterXmlResponse yamlLoadFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataYamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlSaveFasterXmlResponse yamlSaveFasterXmlData(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().dataYamlSaveFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}

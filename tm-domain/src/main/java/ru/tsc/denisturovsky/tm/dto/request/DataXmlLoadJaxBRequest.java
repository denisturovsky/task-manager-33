package ru.tsc.denisturovsky.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataXmlLoadJaxBRequest extends AbstractUserRequest {

    public DataXmlLoadJaxBRequest(@Nullable String token) {
        super(token);
    }

}
